const HTMLWebpackPlugin = require('html-webpack-plugin')
const { join } = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const { HotModuleReplacementPlugin } = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: ['babel-polyfill', join(__dirname, 'app.js')],
  output: {
    path: join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devServer: {
    port: 80,
    contentBase: join(__dirname, 'src/'),
    publicPath: '/'
  },
  plugins: [
    new VueLoaderPlugin(),
    new CleanWebpackPlugin(),
    new HotModuleReplacementPlugin(),
    new HTMLWebpackPlugin({
      title: 'Joblocal Jobs UI',
      filename: 'index.html',
      template: './index-template.html'
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [{
          loader: 'file-loader?outputPath=images/',
          options: { esModule: false, outputPath: "images/" }
        }]
      }
    ]
  }
}
