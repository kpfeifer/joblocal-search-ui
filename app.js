import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap' //Importing JavaScript https://getbootstrap.com/docs/4.1/getting-started/webpack/
import 'bootstrap/dist/css/bootstrap.min.css' //Importing Compiled CSS
import 'bootstrap-vue/dist/bootstrap-vue.css'



import App from './src/vue/App.vue'

Vue.use(BootstrapVue)


new Vue({
    render: (createEl) => createEl(App)
}).$mount('#site')
