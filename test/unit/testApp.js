import { mount } from '@vue/test-utils'
import App from '../../src/vue/App.vue'

import Vue from 'vue'
Vue.config.silent = true;


describe('App', () => {

  it('has a data function', () => {
    expect(typeof App.data).toBe('function')
  })

  it('searchQuery is a empty string', () => {
    const data = App.data()
    expect(data.searchQuery).toBe('')
  })

  it('start api request & change loading status on search button click', () => {
    const wrapper = mount(App)

    const button = wrapper.find('b-button')


    expect(button.exists()).toBe(true)

    expect(wrapper.vm.loading).toBe(false)

    button.trigger('click')

    expect(wrapper.vm.loading).toBe(true)
  })

  it('check api response', async () => {
    const wrapper = mount(App)
    await wrapper.vm.startSearch()

    expect(wrapper.vm.searchResult).not.toBe(null)

    expect(typeof wrapper.vm.searchResult).toBe('object')
    expect(wrapper.vm.searchResult).toHaveProperty('data');
    expect(wrapper.vm.searchResult).toHaveProperty('included');

  })
})
